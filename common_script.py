import os
import sys
import pandas as pd
import numpy as np
from datetime import date, timedelta, datetime
import time
from sqlalchemy import create_engine
#import psycopg2 as pg
#from sqlalchemy import create_engine
PATH = '/data1/delhivery/Redshift_Dashboards/fads/constant'
#path = os.path.dirname(os.path.realpath(__file__))
#PATH = '/'.join(path.split('/')[:-1])
sys.path.append(PATH)

### importing Redshift credentials #####
from config import dsn_database,\
                   dsn_hostname,\
                   dsn_port,\
                   dsn_uid,\
                   dsn_pwd

### importing Postgres credentials #####

from config import dsn_database1,\
                   dsn_hostname1,\
                   dsn_port1,\
                   dsn_uid1,\
                   dsn_pwd1,\
                   table1,\
                   table2,\
                   table3


start = datetime.now() - timedelta(days = 30)

pattern = '%d/%m/%Y %H:%M:%S'
start_datetime = '%s/%s/%s %s:%s:%s' % (start.day, start.month,start.year,start.hour,start.minute,start.second)
start_datetime = int(time.mktime(time.strptime(start_datetime, pattern)))
start_datetime *= 1000000
start_datetime = str(start_datetime)
print(start_datetime)

conn_string_1 = "postgresql://%s:%s@%s:%s/%s" % (dsn_uid,dsn_pwd,dsn_hostname,dsn_port,dsn_database)
engine_1 = create_engine(conn_string_1)

nsls = pd.read_csv('/data1/delhivery/Redshift_Dashboards/fads/data/nsls_amazon.csv')

QUERY = """select _wbn as wbn,date(last_scan_date) as first_dispatch_date,"_cs.ss" as cs_ss,"_cs.st"
as cs_st,_cn as cn,_occ as occ, _cnc as cnc,_mot as mot,_pt as pt,_st as state,_rgn as region,"_cs.nsl" as nsl,"_cs.sr" 
as remark,_cl as client from (select _wbn,"_cs.ss","_cs.st","_cs.sd" + interval '330 minutes' as
last_scan_date,_oc,_cn,_occ,_cnc,_mot,_pt,_st,_rgn,
row_number() over (partition by _wbn order by _action_date asc) as row, "_cs.nsl","_cs.sr",_cl from package_scan_latest
where _action_date >= %s and _pt in ('COD','Pre-paid') and "_cs.nsl" in %s and _pdt <> 'Heavy') where date(last_scan_date) = current_date-1 and row = 1;"""% (start_datetime,str(tuple(nsls.nsl)))

data = pd.read_sql(QUERY,engine_1)

data = data[~(data['cn'] == 'NSZ')]

last_scan_date_query = """select wbn,last_status,status_type,date(last_date) as dt from (select _wbn as wbn,"_cs.ss" as
last_status,"_cs.st" as status_type,"_cs.sd" + interval '330 minutes' as last_date,row_number() over (partition by _wbn order 
by _action_date desc) as row from package_scan_latest
where _action_date > %s) where row = 1 and date(last_scan_date) = current_date-1""" %(start_datetime)

last_scan_date = pd.read_sql(last_scan_date_query,engine_1)

data = pd.merge(data,last_scan_date,on=['wbn'],how = 'left')
data['fdds_flag'] = np.where((data['cs_ss'] == 'Delivered') | ((data['last_status'] == 'Delivered') &
                                                               (data['dt'] == data['first_dispatch_date'])),1,0)
data['return_flag'] = np.where((data['last_status'] == 'RTO') | (data['status_type'] == 'RT'),1,0)
data['region'] = data['region'].astype(str)
data['cnc'] = data['cnc'].astype(str)
data1 = data.groupby(['first_dispatch_date','cn','occ','cnc','mot','pt','state','region','nsl','remark','client',
                      'fdds_flag','return_flag'])['wbn'].count().reset_index()
conn_string_2 = "postgresql://%s:%s@%s:%s/%s" % (dsn_uid1,dsn_pwd1,dsn_hostname1,dsn_port1,dsn_database1)
engine_2 = create_engine(conn_string_2)
data1.to_sql('fads_all_clients_copy',engine_2,if_exists = 'append',index = False)

data2 = data[data['client']== 'AMAZON'].reset_index()

data2 = data2[['wbn','first_dispatch_date','cn','occ','cnc','mot','pt','state','region', 'nsl','remark','fdds_flag','return_flag']]

data2.to_sql('fads_amazon_copy',engine_2,if_exists = 'append',index = False)